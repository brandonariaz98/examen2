package com.example.impuesto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ReciboNomina recibo;
    EditText txtnombre;
    Button btnentrar;
    Button btnsalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtnombre = (EditText) findViewById(R.id.txtnombre);
        btnentrar = (Button) findViewById(R.id.btnentrar);
        btnsalir = (Button) findViewById(R.id.btnsalir);

        recibo = new ReciboNomina();

        btnentrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtnombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this, "Falta capturar nombre", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);

                    intent.putExtra("nombre", nombre);

                    Bundle objeto = new Bundle();

                    objeto.putSerializable("recibo",recibo);

                    intent.putExtras(objeto);

                    startActivity(intent);
                }
            }
        });

        btnsalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
